#include "TestClasses.h"

int DerivedVirtual::getData() const
{
	return this->data % time(nullptr);
}

int Base::getData() const
{
	return this->data % time(nullptr);
}
