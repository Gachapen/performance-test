#include <chrono>
#include <cstdio>
#include <fstream>

#include "TestClasses.h"
#include "config.h"

int main()
{
	Base* test = new Derived();

	auto startPoint = std::chrono::high_resolution_clock::now();

	int count = 0;

	for (long int i = 0; i < NUM_RUNS; i++)
	{
		count += test->getData();
	}

	auto endPoint = std::chrono::high_resolution_clock::now();
	auto duration = endPoint - startPoint;

	printf("%li", duration.count());

	std::ofstream output("tmp");
	output << count;

	return 0;
}
