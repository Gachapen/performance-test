normal=$(./normal)
virtual=$(./virtual)
inline=$(./inline)

normalSpeedup=$(echo "$virtual / $normal" | bc -l | awk '{printf "%f", $0}')
inlineSpeedup=$(echo "$virtual / $inline" | bc -l | awk '{printf "%f", $0}')

echo "Virtual used $virtual ns"
echo "Normal used $normal ns and is $normalSpeedup faster."
echo "Inline used $inline ns and is $inlineSpeedup faster."
