#ifndef TESTCLASSES_H_
#define TESTCLASSES_H_

#include <ctime>

struct BaseVirtual
{
	virtual ~BaseVirtual()
	{}

	virtual int getData() const = 0;
};

struct DerivedVirtual: public BaseVirtual
{
	int data;

	DerivedVirtual():
		data(1)
	{}

	int getData() const override;
};

struct Base
{
	int data;

	Base(int data):
		data(data)
	{}

	int getData() const;
};

struct Derived: public Base
{
	Derived():
		Base(1)
	{}
};

struct BaseInline
{
	int data;

	BaseInline(int data):
		data(data)
	{}

	inline int getData() const
	{
		return this->data % time(nullptr);
	}
};

struct DerivedInline: public BaseInline
{
	DerivedInline():
		BaseInline(1)
	{}
};

#endif /* TESTCLASSES_H_ */
